///CSE002
///Zehong Deng
// Date:2018-12-6
//This program illustrate the selection sord

import java.util.Arrays;
public class SelectionSortLab10{
  
  public static void main (String[] args){
    int[] myArrayBest = {1,2,3,4,5,6,7,8,9};
    int[] myArrayWorst = {9,8,7,6,5,4,3,2,1};
    int Best = selectionSort(myArrayBest) ;
    System.out.println("The total number of operations performed on the sorted array: "
+ Best);
    int Worst = selectionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the reverse sorted array: "
+ Worst);
  }
  
  public static int selectionSort(int[] list){
    System.out.println(Arrays.toString(list));
    int iter = 0;
    int temp;
    for (int i = 0; i < list.length-1; i ++){
      iter ++;
      int cMin = list[i];
      int cMinIndex = i;
      for(int j = i +1; j <list.length; j++){
        iter++;
        
        if (list[j]<= cMin){
          cMin = list[j];
          cMinIndex = j;
        }
      }
      
      if(cMinIndex != i){
        temp = list[i];
        list[i] = cMin;
        list[cMinIndex] = temp;
      
      } 
      //output each step
      System.out.println(Arrays.toString(list));
      
       
    }
    return iter;
    
  }
  
  
  
  
  
}
