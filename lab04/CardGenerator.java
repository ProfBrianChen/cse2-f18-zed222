//////////////
//// CSE 002-310 
//// lab04
/// name: Zehong Deng
/// Date: 2018-09-20
/*
 *This program is used to generate random card 
 */ 

import java.lang.Math;
/*After you generate a random number, create two String variables: a String corresponding to the name of the suit and a String corresponding to the identity of the card.
Use if statements to assign the suit name.
Use a switch statement to assign the card identity.
Print out the name of the randomly selected card.
*/

public class CardGenerator{
   public static void main(String args[]){
    int number = (int)(Math.random()*52)+1;
    System.out.println("The generated number is " + number);
    
    //create two String variables: a String corresponding to the name of the suit and a String corresponding to the identity of the card.
    String suitName = " ";
    String identity = " ";
    
    //assign the suit name for the card
    if ( number > 1 && number <= 13 ){
      suitName = "diamonds";
    }
    else if ( number > 13 && number <= 26 ){
      suitName = "clubs";
    }
    else if ( number > 26 && number <= 39 ){
      suitName = "hearts";
    }
    else if ( number > 39 && number <= 52 ){
      suitName = "spades";
    }
    System.out.println("The suit is " + suitName);
    
    //assign the card identity 
    int modolus;
    modolus = number % 13;
    System.out.println ("The modolus is "+ modolus);
    
    switch( modolus ){
      case 0:
        identity = "k";
        break;
      case 1:
        identity = "ace";
        break;
      case 2:
        identity = "2";
        break;
      case 3:
        identity = "3";
        break;
      case 4:
        identity = "4";
        break;
      case 5:
        identity = "5";
        break;
      case 6:
        identity = "6";
        break;
      case 7:
        identity = "7";
        break;
      case 8:
        identity = "8";
        break;
      case 9:
        identity = "9";
        break;
      case 10:
        identity = "10";
        break;
      case 11:
        identity = "J";
        break;
      case 12:
        identity = "Q";
        break;
      default:
        System.out.println("The card may not be in the suit.");
    }
       System.out.println("The identity is "+ identity);
      
    }//end of the main   
  }//end of the class
