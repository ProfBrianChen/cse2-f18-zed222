//// CSE 002-310
//// hw06
/// name: Zehong Deng
/// Date: 2018-10-21
/*
 *This program uses nested loops to print a X
 * There is test output in the process, which started with "//Test Partial Output"
 */
import java.lang.Math;
import java.util.*;


public class EncryptedX {

    public static void main(String[] args) {
        //Ask the user the time it should generate the hands
        Scanner scan = new Scanner (System.in);
        System.out.print("Please enter the size (int from 0-100): ");

        //Check the input value type should be an int
        while(scan.hasNextInt() == false ){
            System.out.println("Invalid input type. Need a int: ");
            scan.next();
        }
        int size = scan.nextInt();

        //Check the input value in the range 0-100
        while(size >100 || size < 0){
            System.out.println("Input is out of range. Please re-enter: ");
            size = scan.nextInt();
        }

        //Partical Test
        System.out.println("Size: " + size);

        int half = size/2;
        int halfRow = size -1;

        //Row 1 to the middile row
        for(int row = 1; row <= (size/2); row ++){
            //part1
            for(int part1 = 1; part1 < row; part1++){
                System.out.print("*");
            }

            //part2
            System.out.print(" ");

            //part3
            for(int part3 = 1; part3 <= halfRow-2*(row-1); part3 ++){
                System.out.print("*");
            }

            //part4
            System.out.print(" ");

            //part5
            for(int part1 = 1; part1 < row; part1++){
                System.out.print("*");
            }

            //Go into the nest row
            System.out.println();

        }

        //the middle row
        for(int a =1 ;a <= size/2; a++) {
            System.out.print("*");
        }
        System.out.print(" ");
        for(int a =1 ;a <= size/2; a++) {
            System.out.print("*");
        }
        System.out.println();

        //the middle row to the last row
        for(int row = 1; row <= half; row ++){
            //part1
            for(int part1 = 1; part1 <= half-row; part1++){
                System.out.print("*");
            }

            //part2
            System.out.print(" ");

            //part3
            int counter = 1;
            for(int part3 = 1; part3 <= halfRow-2*(half-row); part3 ++){
                System.out.print("*");
                counter +=2;
            }

            //part4
            System.out.print(" ");

            //part5
            for(int part1 = 1; part1 <= half-row; part1++){
                System.out.print("*");
            }

            //Go into the nest row
            System.out.println();

        }

    }
}
