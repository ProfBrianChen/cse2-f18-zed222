//////////////
//// CSE 002-310 
//// hw02: Arithmetic Calculations
/// name: Zehong Deng
/// Date: 2018-09-06
/*
 *This program is used to calculate the costs of the shopping items, 
 *including the PA sales of 6%.
 */


public class Arithmetic{
  
  public static void main(String args[]){
    /* Part 1:
    Total cost of each kind of item (i.e. total cost of pants, etc)
    */
    
    // Pants
    int numPants;        //Number of pairs of pants
    numPants = 3;
    double pantsPrice;   //Cost per pair of pants
    pantsPrice = 34.98;
    double pantsCosts = numPants * pantsPrice;   //total costs of pants without tax

    // Sweatshirts
    int numShirts;       //Number of sweatshirts
    numShirts = 2;
    double shirtPrice;   //Cost per shirt
    shirtPrice = 24.99;
    double shirtCosts = numShirts * shirtPrice; //total costs of sweatshirts without tax

    // Belts
    int numBelts;        //Number of belts
    numBelts = 1;
    double beltsPrice;     //cost per belt
    beltsPrice = 33.99;
    double beltsCosts = numBelts * beltsPrice; //total costs of belts without tax



    /* Part 2:
    Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
    */

    //the tax rate
    double paSalesTax;   
    paSalesTax = 0.06;

    //sales tax charged on pants
    double pantsTax = pantsCosts * paSalesTax;

    //sales tax charged on sweatshirts
    double shirtTax = shirtCosts * paSalesTax;

    //sales tax charged on belts
    double beltsTax = beltsCosts * paSalesTax;



    /* Part 3:
    Total cost of purchases (before tax)
    */
    double totalCosts;
    totalCosts = pantsCosts + shirtCosts + beltsCosts;



    /* Part 4:
    Total sales tax
    */
    double totalTax;
    totalTax = pantsTax + shirtTax + beltsTax;



    /* Part 5:
    Total paid for this transaction, including sales tax.
    */
    
    double totalPaid = totalCosts + totalTax;
    // Remain two digits
    totalPaid = Math.round(totalPaid *100.0)/100.0;  
    
    //Output the data
    System.out.println ("The total paid of all items which including 6% PA tax is $ " + totalPaid + ".");
      
    
    
  }
}

