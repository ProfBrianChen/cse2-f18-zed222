//// CSE 002-310
//// hw08
/// name: Zehong Deng
/// Date: 2018-11-25
/*
 *This homework manipulate arrays. 
 * It includes three methods called in the main method, randomInput(), delete(list,pos), and remove(list,target).
 */

import java.util.Scanner;

public class RemoveElements {

    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        int num[]=new int[10];
        int newArray1[];
        int newArray2[];
        int index,target;
        String answer="";
        do{
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput();
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num,index);
            String out1="The output array is ";
            out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num,target);
            String out2="The output array is ";
            out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer=scan.next();
                }while(answer.equals("Y") || answer.equals("y"));
            }


    public static String listArray(int num[]){
        String out="{";
        for(int j=0;j<num.length;j++){
            if(j>0){
                out+=", ";
            }
            out+=num[j];
        }
        out+="} ";
        return out;
    }//End of the listArray Method

    public static int[] randomInput(){
        int[]list1= new int[10];
        for(int i = 0; i < list1.length; i ++){
            list1[i]=(int)(Math.random()*10);
        }
        return list1;
    }//end of the randomInput method

    public static int[] delete(int[] list, int index){
        int[]list2= new int[list.length-1];
        int k =0;           //counter for the list2 (the new array after delete)
        for(int i = 0; i < list.length;i++){    //go through every int in the list (original array
            if(i != index){
                list2[k]=list[i];
                k++;
            }
        }
        return list2;
    }//End of the delete method

    public static int[] remove(int[] list, int tar){
        int appear = 0;         // A counter for calculating the number of appearance of target in list (the original array)
        for(int i = 0; i < list.length; i ++){
            if(list[i]==tar){
                appear++;
            }
        }
        int size = list.length- appear;
        int[] list3= new int[size];

        //Do the remove: fill in the non-target into list3 (the new list)
        int k = 0;                          //counter for the list3 (the new array after delete)
        for(int i = 0; i < list.length;i++){    //go through every int in the list (original array)

            if(list[i]!=tar){
                list3[k]=list[i];
                k++;
            }
        }
        return list3;

    }//End of the remove method
    }//End of the class





