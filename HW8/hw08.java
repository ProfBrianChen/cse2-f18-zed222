//// CSE 002-310
//// hw08
/// name: Zehong Deng
/// Date: 2018-11-13
/*
 *This homework  manipulate arrays and in writing methods that have array parameters. 
 * It includes three method(printArray,shuffle,getHand) for poker cards.
 */

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class hw08 {
    public static void main(String[] args) {
          Scanner scan = new Scanner(System.in);
                //suits club, heart, spade or diamond
          String[] suitNames={"C","H","S","D"};
          String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
          String[] cards = new String[52];
          String[] hand = new String[5];
          int numCards = 5;
          int again = 1;
          int index = 51;
          for (int i=0; i<52; i++){
              cards[i]=rankNames[i%13]+suitNames[i/13];
              //System.out.print(cards[i]+" ");
          }
          System.out.println();
          printArray(cards);
          System.out.println();
          System.out.println("Shuffled");
          shuffle(cards);
          printArray(cards);
          while(again == 1){
              //If index is smaller than numCards, we generate a new set of card.
              if(index<numCards){
                  System.out.println("Cards are run out. Generate a new set of cards.");
                  shuffle(cards);
                  index = 51;
              }
              System.out.println();
              hand = getHand(cards,index,numCards);
              printArray(hand);
              index -= numCards;
              System.out.println();
              System.out.println("Enter a 1 if you want another hand drawn");
              again = scan.nextInt();
          }
      }//End of main
      
      /*
      *The method printArray(list) takes an array of Strings and prints out each element seperated by a space
      */
      public static void printArray(String[]input){
          for (int i =0; i <input.length;i ++){
          System.out.print(input [i]+ " ");
          }
      }//End of method printArray

      /*
      *The method shuffle(list) shuffles the elements of the list by continuously randomize an index number of list (that is not zero) 
      *and swaps the element at that index with the first element (at index 0)
      */
      public static void shuffle (String[]input){
              String[] neww = input;
              int j = 0;
              int randomIndex = 0;
              //while( j <=50){//shuffle more than 50 times
              for (int k = 0; k < 52; k ++) {
                      randomIndex = (int) (Math.random() * 52);

                  //switch two String in the array
                  String temp = neww[k];
                  neww[k] = neww [randomIndex];
                  neww[randomIndex] = temp;
              }

              //Print out the shuffled cards

              /*for(int l = 0; l < neww.length;l ++){
                  System.out.print(neww[l]+" ");
              }*/
          }//End of method shuffle


      /*
      *The method getHand(list, index, numCards) returns an array that holds the number of cards specified in numCards.
      */
     public static String[] getHand(String[]list, int index, int numCards){
          String[] Hand = new String[numCards];
          for (int a = 0; a < numCards; a ++){
              int b = index - a ;
              Hand[a]= list[b];
          }

          //Prompt the user that now we provide the hand
          System.out.println("Hand");
          /*for (int c = 0; c< Hand.length; c++){
              System.out.print(Hand[c] + " ");
          }*/
          return Hand;
         }//End of method getHand

  }//End of the Class






