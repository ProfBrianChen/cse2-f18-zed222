//// CSE 002-310
//// lab06
/// name: Zehong Deng
/// Date: 2018-10-11
/*
 *This program uses nest loop for pyramid print out
 */
import java.util.Scanner;

public class Lab06 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter the length (1-10): ");
        while(scan.hasNextInt() == false){
            System.out.println("Invalid input.Please reenter from 1-10: ");
            scan.next();
        }
        int length =  scan.nextInt();
        while(length < 1 || length >10){
            System.out.println("Invalid input range.Please reenter from 1-10: ");
            length=scan.nextInt();
        }


        System.out.println("Pattern A");
        for(int cou1 = 1; cou1 <= length; cou1++ ){
            for(int cou2 = 1; cou2 <= cou1; cou2++){
                System.out.print(cou2 + " ");
            }
            System.out.println();
        }

        int length2 = length;
        System.out.println("Pattern B");
        for(int cou1 = 1; cou1 <= length; cou1++ ){
            for(int cou2 = 1; cou2 <= length2; cou2++){
                System.out.print(cou2 + " ");

            }
            length2--;
            System.out.println();
        }

        System.out.println("Pattern C");
        for(int cou1 = 1; cou1 <= length; cou1++ ){
            for(int cou3 = 1; cou3 <= length-cou1;cou3++){
                System.out.print(" ");
            }
            for(int cou2 = cou1; cou2 >= 1; cou2--){
                System.out.print(cou2);
            }
            System.out.println();
        }

        int length3 =length;
        System.out.println("Pattern D");
        for(int cou1 = 1; cou1 <= length; cou1++ ){
            for(int cou2 = length3; cou2 >= 1; cou2--){
                System.out.print(cou2 + " ");
            }
            length3 --;
            System.out.println();
        }



    }
}
