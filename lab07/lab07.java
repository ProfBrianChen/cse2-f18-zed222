//// CSE 002-310
//// lab07
/// name: Zehong Deng
/// Date: 2018-10-26
/*
 *Methods allow software developers to modularize and reuse their code in many contexts, 
 reducing the chance for bugs and increasing productivity.  
 This lab will give the basic experience in declaring and calling methods. 
 */
import java.util.Random;
import java.util.Scanner;


    public class lab07 {
        public static String adjectives(int randomInt1) {
            String adj ;
            switch (randomInt1){
                case 1:
                    adj = "happy";
                    break;
                case 2:
                    adj = "sad";
                    break;
                case 3:
                    adj = "lazy";
                    break;
                case 4:
                    adj = "helpful";
                    break;
                case 5:
                    adj = "smart";
                    break;
                case 6:
                    adj = "lovely";
                    break;
                case 7:
                    adj = "beautiful";
                    break;
                case 8:
                    adj = "crazy";
                    break;
                case 9:
                    adj = "mad";
                    break;
                case 10:
                    adj = "handsome";
                    break;
                default:
                    adj = "Wrong";
                    break;
            }
            return adj;
        }

        public static String nouns1(int randomInt2) {
            //Random randomGenerator = new Random();

            String nouns1 ;
            switch (randomInt2){
                case 1:
                    nouns1 = "fox";
                    break;
                case 2:
                    nouns1 = "Jack";
                    break;
                case 3:
                    nouns1 = "Mary";
                    break;
                case 4:
                    nouns1 = "god";
                    break;
                case 5:
                    nouns1 = "rabbit";
                    break;
                case 6:
                    nouns1 = "dog";
                    break;
                case 7:
                    nouns1 = "cat";
                    break;
                case 8:
                    nouns1 = "professor";
                    break;
                case 9:
                    nouns1 = "student";
                    break;
                case 10:
                    nouns1 = "cleaner";
                    break;
                default:
                    nouns1 = "dancer";
                    break;
            }
            return nouns1;
        }

        public static String pastTense(int randomInt3) {
            //Random randomGenerator = new Random();

            String past ;
            switch (randomInt3){
                case 1:
                    past = "eat";
                    break;
                case 2:
                    past = "smiled to";
                    break;
                case 3:
                    past = "run to";
                    break;
                case 4:
                    past = "walked toward";
                    break;
                case 5:
                    past = "danced for";
                    break;
                case 6:
                    past = "sang";
                    break;
                case 7:
                    past = "listened to";
                    break;
                case 8:
                    past = "killed";
                    break;
                case 9:
                    past = "joked";
                    break;
                case 10:
                    past = "passed";
                    break;
                default:
                    past = "spoke to";
                    break;
            }
            return past;
        }

        public static String nouns2(int randomInt4) {
            //Random randomGenerator = new Random();

            String nouns2;
            switch (randomInt4) {
                case 1:
                    nouns2 = "singer";
                    break;
                case 2:
                    nouns2 = "it";
                    break;
                case 3:
                    nouns2 = "him";
                    break;
                case 4:
                    nouns2 = "her";
                    break;
                case 5:
                    nouns2 = "us";
                    break;
                case 6:
                    nouns2 = "them";
                    break;
                case 7:
                    nouns2 = "stuff";
                    break;
                case 8:
                    nouns2 = "seller";
                    break;
                case 9:
                    nouns2 = "officer";
                    break;
                case 10:
                    nouns2 = "police";
                    break;
                default:
                    nouns2 = "teacher";
                    break;
            }
            return nouns2;
        }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter 0 is you want a sentence: ");
        int input = scan.nextInt();

        Random randomGenerator = new Random();
        int randomInt1 = randomGenerator.nextInt(10);
        int randomInt2 = randomGenerator.nextInt(10);
        int randomInt3 = randomGenerator.nextInt(10);
        int randomInt4 = randomGenerator.nextInt(10);


        while (input == 0) {
            randomInt1 = randomGenerator.nextInt(10);
            randomInt2 = randomGenerator.nextInt(10);
            randomInt3 = randomGenerator.nextInt(10);
            randomInt4 = randomGenerator.nextInt(10);

            String adj = adjectives(randomInt1);
            String nouns1 = nouns1(randomInt2);
            String pastTense = pastTense(randomInt3);
            String nouns2 = nouns2(randomInt4);

            System.out.println("The " + adj + " " + nouns1 + " " + pastTense + "the " + nouns2 + "."  );
            System.out.println("Would you like to add another sentence: ");
            System.out.println("Enter 0 is you want another one; enter 1 to quit: ");
            input = scan.nextInt();
        }
    }





}