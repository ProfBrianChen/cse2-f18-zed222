//////////////
//// CSE 002-310 
//// hw04-CrapsIf
/// name: Zehong Deng
/// Date: 2018-09-22
/*
 *This program is used for a classic casino game, Craps, 
 *involves the rolling of two six sided dice and evaluating the result of the roll.  
 *Among other things, craps is notable for producing slang terminology for describing the outcome of a roll of two dice.
 *This algorithm uses if statement.
 */ 

import java.lang.Math;
import java.util.Scanner;


public class CrapsIf{
   public static void main(String args[]){
     /*Ask the user wheter to randomly cast dice
      or to state the two dice they want to evaluate
      */
     Scanner myScanner = new Scanner( System.in );
     System.out.println("If you want to randomly cast dice, please enter 1.");
     System.out.print("If you want to state two dice, please enter 2: ");
     int inputWay = myScanner.nextInt();
     
     int number1 = 0;
     int number2 = 0;
     int score = 0;
     String slang = " ";
     
     /*
     If the user chooses to randomly cast dice, 
     generate two random numbers from 1-6, inclusive, 
     representing the outcome of the two dice cast in the game.
     */
     if (inputWay == 1 ) {
        number1 = (int)(Math.random()*6)+1;
        number2 = (int)(Math.random()*6)+1;
     }
     
     /*
     If the user wants to provid dice, 
     use the Scanner twice to ask the user for two integers from 1-6, inclusive.  
     Assume the user always provides an integer, but check if the integer is within the range.
     */
     else if (inputWay == 2) {
            System.out.println("Please enter the 1st dice number (from 1 to 6): ");
            number1 = myScanner.nextInt();
            //Check the number1 is in the range
            if (number1 < 1 || number1 > 6 ) {
            System.out.println("The number1 is not in the range");
            //If the number1 is not in the range, exit the program.
            System.exit(0);
            }
       
            System.out.println("Please enter the 2nd dice number (from 1 to 6): ");
            number2 = myScanner.nextInt();
            //check wether the number2 is in the range
            if (number2 < 1 || number2 >6) {
            System.out.println("The number2 is not in the range");
            //If the number is not in the range, exit the program
            System.exit(0);
            }
     }
     else{
       System.out.println("You choose a wrong way to input value");
       System.exit(0); 
     }
       

     //Print out the two numbers for the user to see
     System.out.println("The generated number is " + number1 + " and " + number2);
     score = number1 + number2;
     
     /*
     Determine the slang terminology of the outcome of the roll
     */
     //score = 2
     if (score == 2) {
       slang = "Snake Eyes";
     }
    
     //score = 3
     else if (score == 3) {
       slang = "Ace Deuce";
     }
     
     //score = 4
     else if (score == 4) {
        if (number1 == 1 || number2 == 1) {
            slang = "Easy Four";
        }
        else {
         slang = "Hard Four";
        }
     }
  
     //score = 5
     else if (score == 5) {
        if (number1 == 1 || number2 == 1) {
            slang = "Fever Five";
        }
        else{
            slang = "Fever five";
        }
     }

     //score = 6
     else if (score == 6) {
        if (number1 == 1 || number2 == 1) {
            slang = "Easy Six";
        }
        else if (number1 == 2 || number2 == 2) {
            slang = "Easy six";
        }
        else {
            slang = "Hard six";
        }
     }
     
     //score = 7
     else if (score == 7) {
           slang = "Seven out";
     }
   
     //score = 8
     else if (score == 6){
         if (number1 == 4 || number2 == 4 ) {
             slang = "Hard Eight";
         }
         else{
             slang = "Easy Eight";
         }
      }
     
     //score = 9
     else if (score == 9) {
        slang = "Nine";
     }

     //score = 10
     else if (score == 10){
         if (number1 == 5 || number2 == 5 ){
             slang = "Hard Ten";
         }
         else{
             slang = "Easy Ten";
         }
     }
     
     //score = 11
     else if (score == 11) {
         slang = "Yo-leven";
     }
     
     //score = 12
     else if (score == 12){ 
         slang = "Boxcars";
     }
     
     else{
       System.out.println("It is impossible for this situation.");
     }
   
     /*
     Print out the slang terminology
     */
     System.out.println("The slang terminology is "+ slang + ".");
    
     /*
     End of the Program
     Exit
     */
     System.exit(0);
    
    
    }//end of the main   
  }//end of the class





