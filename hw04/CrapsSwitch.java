//////////////
//// CSE 002-310 
//// hw04-CrapsSwitch
/// name: Zehong Deng
/// Date: 2018-09-22
/*
 *This program is used for a classic casino game, Craps, 
 *involves the rolling of two six sided dice and evaluating the result of the roll.  
 *Among other things, craps is notable for producing slang terminology for describing the outcome of a roll of two dice.
 *This algorithm uses switch statement.
 */ 

import java.lang.Math;
import java.util.Scanner;


public class CrapsSwitch{
   public static void main(String args[]){
     /*Ask the user wheter to randomly cast dice
      or to state the two dice they want to evaluate
      */
     Scanner myScanner = new Scanner( System.in );
     System.out.println("If you want to randomly cast dice, please enter 1. ");
     System.out.print("If you want to state two dice, please enter 2: ");
     int inputWay = myScanner.nextInt();
     
     int number1 = 0;
     int number2 = 0;
     int score = 0;
     String slang = " ";
     /*
     If the user chooses to randomly cast dice, 
     generate two random numbers from 1-6, inclusive, 
     representing the outcome of the two dice cast in the game.
     */
     switch (inputWay){
        case 1:
        number1 = (int)(Math.random()*6)+1;
        number2 = (int)(Math.random()*6)+1;
        break;
     
     
     /*
     If they want user provided dice, 
     use the Scanner twice to ask the user for two integers from 1-6, inclusive.  
     Assume the user always provides an integer, but check if the integer is within the range.
     */
        case 2:
        System.out.println ( "Please enter the first dice number (from 1 to 6): ");
        number1 = myScanner.nextInt();
        //Check the number1 is in the range
        switch(number1){
          case 1:
          break;
          case 2:
          break;
          case 3:
          break;
          case 4:
          break;
          case 5:
          break;
          case 6:
          break;
          default:
            System.out.println("The number1 is not in the range");
            //If the number1 is not in the range, exit the program.
            System.exit(0);
          break;
          }

       System.out.println ( "Please enter the second dice number (from 1 to 6): ");
        number2 = myScanner.nextInt();
         //Check the number2 is in the range
        switch(number2){
          case 1:
          break;
          case 2:
          break;
          case 3:
          break;
          case 4:
          break;
          case 5:
          break;
          case 6:
          break;
          default:
            System.out.println("The number2 is not in the range");
            //If the number2 is not in the range, exit the program.
            System.exit(0);
          break;
          }
        break;
    
        default: 
          System.out.println("You choose a wrong way to input value");
          System.exit(0);
        break;
     }
     
   

     
     //Print out the two numbers for the user to see
     System.out.println ("The generated number is " + number1 + " and " + number2);
     score = number1 + number2;
     
     /*
     Determine the slang terminology of the outcome of the roll
     */
     //score = 2
     switch (score){
         case 2:
         slang = "Snake Eyes";
         break;
     
    
     //score = 3
         case 3:
         slang = "Ace Deuce";
         break;
     
     
     //score = 4
         case 4:
         switch (number1){
            case 1:
            slang = "Easy Four";
            break;
              
            case 3:
            slang = "Easy Four";
            break;
  //Right or not?
            default:
            slang = "Hard Four";
            break;
            }
          break;
  
      

  
     //score = 5
        case 5:
        switch(number1){
            case 1:
            slang = "Fever Five";
            break;
              
            default:
            slang = "Fever five";
            break;
           }
        break;

     //score = 6
        case 6:
        switch (number1){
          case 1:
          slang = "Easy Six";
          break;
            
          case 2:
          slang = "Easy six";
          break;
       
          default:
          slang = "Hard six";
          break;
        }
        break;
     
     //score = 7
        case 7:
           slang = "Seven out";
        break;
   
     //score = 8
        case 8:
          switch(number1){
          case 4:
          slang = "Hard Eight";
          break;
        
          default:
          slang = "Easy Eight";
          break;
          }
        break;
          
     
     //score = 9
        case 9:
        slang = "Nine";
        break;

     //score = 10
        case 10:
          switch(number1){
            case 5:
            slang = "Hard Ten";
            break;
          
        default:
             slang = "Easy Ten";
             break;
          }
     
     //score = 11
        case 11:
        slang = "Yo-leven";
        break;
     
     //score = 12
        case 12:
        slang = "Boxcars";
        break;
        
        default:
        System.out.println("The input of two values are wrong.");
        break;
          
     /*
     End of the Program
     Exit
     */
    
   
          
      }//end of the switch of score
     /*
     Print out the slang terminology
     */
     System.out.println ( "The slang terminology is "+ slang + ".");
    
}
       }  //end of the main   
  



