//////////////
//// CSE 002-310 
//// lab05
/// name: Zehong Deng
/// Date: 2018-10-04
/*
Ask the class the user is currently taking:
 ask for the course number, department name, 
 the number of times it meets in a week, the time the class starts, 
 the instructor name, and the number of students.
*/


import java.util.*;


public class ClassInfor{
   public static void main(String args[]){
     Scanner scan = new Scanner(System.in);
    
     
     System.out.println("Course number:");
     while(scan.hasNextInt() == false){
        System.out.println("Invalid input. Need a int for course number:");
        scan.next(); 
     }
     int courseNum = scan.nextInt();  
     
     
     System.out.println("Department name:");
     while(scan.hasNext() == false){
        System.out.println("Invalid input. Need a String:");
        scan.next();   
     }
     String dept = scan.next();  
     
     System.out.println("The number of times it meets in a week");
     while(scan.hasNextInt() == false){
        System.out.println("Invalid input. Need a int for course number:");
        scan.next();  
     }
     int meet = scan.nextInt();   
     
     System.out.println("the time the class starts, such as 1045 means 10:45AM");
     while(scan.hasNextInt() == false){
        System.out.println("Invalid input. Need a String:");
        scan.next();     
     }
     int timeStart = scan.nextInt();
     
     System.out.println("the instructor name");
     while(scan.hasNext() == false){
        System.out.println("Invalid input. Need a String:");
        scan.next();     
     }
     String instructor = scan.next();
     
     System.out.println("the number of students:");
     while(scan.hasNextInt() == false){
        System.out.println("Invalid input. Need a int for course number:");
        scan.next();     
     }
     int stuNum = scan.nextInt();
     /* ask for the course number, department name, 
 the number of times it meets in a week, the time the class starts, 
 the instructor name, and the number of students.
 */
     System.out.println("The number of class is "+ courseNum);
     System.out.println("The name of the department is " + dept);
     System.out.println("The number it meets in a week is " + meet);
     System.out.println("The time it starts " + timeStart);
     System.out.println("The instructor name is "+ instructor);
     System.out.println("The number of students in the class: " + stuNum);
     
     
     
     
   }
}