//////////////
//// CSE 002-310 
//// hw03: Program #2
/// name: Zehong Deng
/// Date: 2018-09-15
/*
 *This program is used to convert the quality of rain into cubic miles 
 */
import java.util.Scanner;

 
public class Pyramid{
  
  public static void main(String args[]){
    /*
    *Input the Data
    */
    Scanner myScanner = new Scanner( System.in );
    //Prompt the user for the square side of the pyramid
    System.out.print("The square side of the pyramid is (input length): ");
    //accept the user input for height of the pyramid
    double side = myScanner.nextDouble();
    //Prompt the user for the rainfall in affected area in the unit of inches
    System.out.print("The height of the pyramid is (input height): ");
    //accept the user input for inchesRainfall
    double height = myScanner.nextDouble();
    
    /*
    *Calculation and Output
    */
    double volume = (height * side * side)/3;
    System.out.println("The volume inside the pyramid is: " + volume);
    
   
  }//end of main method
  
  
}//end of class


