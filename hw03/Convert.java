//////////////
//// CSE 002-310 
//// hw03: Program #1
/// name: Zehong Deng
/// Date: 2018-09-15
/*
 *This program is used to calculate the quantity rainfall and convert the unir into cubic miles  
 */
import java.util.Scanner;

public class Convert{
  
  public static void main(String args[]){
    /*
    *Input the Data
    */
    Scanner myScanner = new Scanner( System.in );
    //Prompt the user for the affected area in acres
    System.out.print("Enter the affected area in acres: ");
    //accept the user input for acresArea
    double acresArea = myScanner.nextDouble();
    //Prompt the user for the rainfall in affected area in the unit of inches
    System.out.print("Enter the rainfall in the affected area: ");
    //accept the user input for inchesRainfall
    double inchesRainfall = myScanner.nextDouble();
   
    /*
    *Calculation and Output
    */
    //Calculate the quantity of rain in gallons
    double gallonsRain = acresArea * inchesRainfall * 27154;
    //Convert the unit from gallons to cubic miles
    double cubicMilesRain = Math.pow(10,-13) * 9.08169 * gallonsRain;
    //Output the result
    System.out.println(cubicMilesRain + "cubic miles");
    
   
  }//end of main method
   
}//end of class