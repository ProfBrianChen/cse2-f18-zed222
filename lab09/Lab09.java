//// CSE 002-310
//// lab07
/// name: Zehong Deng
/// Date: 2018-11-15
/*
 *This lab will illustrate the effect of passing arrays as method arguments.
 */

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class Lab09 {

    public static void main(String[] args) {

        int[] listt = {1,2,5,6,7,8,6,7,2,1};
        int[] array3 = new int[10];

        int[] array0=Copy(listt);
        int[] array1=Copy(listt);
        int[] array2=Copy(listt);
        
        System.out.println("Array0: ");
        inverter(array0);
        print(array0);
        System.out.println();

        System.out.println("Array1: ");
        array1 = inverter2(array1);
        print(array1);
        System.out.println();
        
        System.out.println("Array2: ");
        array3=inverter2(array2);
        print(array3);
        System.out.println();

    }

    public static int[] Copy(int[] list1){
            int[] list2 = new int[list1.length];
        for(int i = 0; i < list1.length;i++){
            list2[i]=list1[i];
        }
            return list2;

        }

        public static void inverter(int[] list1){
        for(int i= 0; i <(list1.length/2);i ++){
            int temp = list1[i];
            list1[i]= list1[list1.length-i-1];
            list1[list1.length-i-1]=temp;
        }
    }

    public static int[] inverter2(int[] list1){

        for(int i= 0; i <(list1.length/2);i ++){
            int temp = list1[i];
            list1[i]= list1[list1.length-i-1];
            list1[list1.length-i-1]=temp;
        }
        int[] list2 = new int[list1.length];
        list2=Copy(list1);

        return list2;

    }

    public static void print(int[] list1){

        /*for (int num : list1)
        {
            System.out.print(num+" ");
        }*/
        for(int i = 0; i < list1.length;i++){
            System.out.print(list1[i]+ " ");
        }
    }

            }//End of the Class






