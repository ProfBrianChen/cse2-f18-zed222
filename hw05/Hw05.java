//// CSE 002-310
//// hw05
/// name: Zehong Deng
/// Date: 2018-10-08
/*
 *This program uses while loops to calculate these probabilities of four hands type.
 * There is test output in the process, which started with "//Test Partial Output"
 */
import java.lang.Math;
import java.util.*;


public class Hw05 {

    public static void main(String[] args) {

        //Ask the user the time it should generate the hands
        Scanner scan = new Scanner (System.in);
        System.out.println("Please enter the times it should generate hands: ");
        //Check the input value type should be an int
        while(scan.hasNextInt() == false){
            System.out.println("Invalid input. Need a int:");
            scan.next();
                }
        int times = scan.nextInt();
        //Test Partial Output
        //System.out.println(times);

        //Declare the counters of four hands
        int fourKind = 0;
        int threeKind = 0;
        int twoPair = 0;
        int onePair = 0;
        int compare = 0;
        String handType = " ";


        //Generate hands
        int currentTimes = 0;
        int cardNum1 = 0;
        int cardNum2 = 0;
        int cardNum3 = 0;
        int cardNum4 = 0;
        int cardNum5 = 0;

        int faceValue1 = 0;
        int faceValue2 = 0;
        int faceValue3 = 0;
        int faceValue4 = 0;
        int faceValue5 = 0;

        //while loop for each 5 cards set
        while(currentTimes < times) {
            //Random Generate five cards in a hand
            cardNum1 = ((int) (Math.random() * 52) + 1);
            cardNum2 = ((int) (Math.random() * 52) + 1);
            cardNum3 = ((int) (Math.random() * 52) + 1);
            cardNum4 = ((int) (Math.random() * 52) + 1);
            cardNum5 = ((int) (Math.random() * 52) + 1);

            //Check that these five cards should be different cards in the poker,if it is different, generate a new one until it is different with the other five cards
            //Check card 1
            while (cardNum1 == cardNum2 || cardNum1 == cardNum3 || cardNum1 == cardNum4 || cardNum1 == cardNum5) {
                cardNum1 = ((int) (Math.random() * 52) + 1);

            }
            //System.out.println("cardNum1 " + cardNum1);   //Test Partial Output

            //Check card 2
            while (cardNum2 == cardNum1 || cardNum2 == cardNum3 || cardNum2 == cardNum4 || cardNum2 == cardNum5) {
                cardNum2 = ((int) (Math.random() * 52) + 1);
            }
            //System.out.println("cardNum2 " + cardNum2);   //Test Partial Output

            //Check card 3
            while (cardNum3 == cardNum2 || cardNum3 == cardNum1 || cardNum3 == cardNum4 || cardNum3 == cardNum5) {
                cardNum3 = ((int) (Math.random() * 52) + 1);
            }
            //System.out.println("cardNum3 " + cardNum3);   //Test Partial Output

            //Check card 4
            while (cardNum4 == cardNum2 || cardNum4 == cardNum3 || cardNum4 == cardNum1 || cardNum4 == cardNum5) {
                cardNum4 = ((int) (Math.random() * 52) + 1);
            }
            //System.out.println("cardNum4 " + cardNum4);    //Test Partial Output

            //Check card 5
            while (cardNum5 == cardNum2 || cardNum5 == cardNum3 || cardNum5 == cardNum4 || cardNum5 == cardNum1) {
                cardNum5 = ((int) (Math.random() * 52) + 1);
            }
            //System.out.println("cardNum5 " + cardNum5);      //Test Partial Output

            //Test Partial Output
            //Print five cards in a number
            //System.out.println("The generated card number is " + cardNum1 + " " + cardNum2 + " " + cardNum3 + " " + cardNum4 + " " + cardNum5);

            //Convert the card numbe (1~52) to the face value of the card

            faceValue1 = cardNum1 % 13;
            faceValue2 = cardNum2 % 13;
            faceValue3 = cardNum3 % 13;
            faceValue4 = cardNum4 % 13;
            faceValue5 = cardNum5 % 13;
            //Test Partial Output
            //System.out.println("The face value: " + faceValue1 + " " + faceValue2 + " " + faceValue3+ " " + faceValue4 + " " + faceValue5);

            //Renew the counter variable compare each time
            compare = 0;

            //do the comparison between each two cards in five cards
            if (faceValue1 == faceValue2)
                compare++;
            if (faceValue1 == faceValue3)
                compare++;
            if (faceValue1 == faceValue4)
                compare++;
            if (faceValue1 == faceValue5)
                compare++;
            if (faceValue2 == faceValue3)
                compare++;
            if (faceValue2 == faceValue4)
                compare++;
            if (faceValue2 == faceValue5)
                compare++;
            if (faceValue3 == faceValue4)
                compare++;
            if (faceValue3 == faceValue5)
                compare++;
            if (faceValue4 == faceValue5)
                compare++;

            //Test Partial Output
            //System.out.println("compare number: " + compare);

            //check for hand type
            if (compare == 6){
                handType = "Four-of-a-kind";
                fourKind++;
            }
            else if (compare == 4) {
                    handType = "Full House";
            }

            else if(compare == 3) {
                handType = "Three-of-a-kind";
                threeKind++;
            }

            else if(compare == 2) {
                handType = "Two-pair";
                twoPair++;
            }

            else if(compare == 1) {
                handType = "One-pair";
                onePair++;
            }

            else if(compare == 0) {
                handType = "String: five cards are different.";
            }



            //Test Partial Output
            //System.out.println("The hand type is " + handType);

            //Renew the condition variable currentTimes
            currentTimes++;
        } //end of While loop
        
      //Test Partial Output
        /*System.out.println("Four hand: " + fourKind);
        System.out.println("Three kind: " + threeKind);
        System.out.println("Two pair: " + twoPair);
        System.out.println("one pair: " + onePair);
        */


        //Calculation
        double proFour = fourKind / (double)times;
        double proThree = threeKind / (double)times;
        double proTwo = twoPair /(double)times;
        double proOne = onePair /(double)times;

        //Output the probability
        //The sum of these four probability may not be 1, since it is possible to have full house and string
        System.out.println("The number of loops: " + times);
        System.out.printf("The probability of Four-of-a-kind: %.3f\n", proFour);
        System.out.printf("The probability of Three-of-a-kind: %.3f\n", proThree);
        System.out.printf("The probability of Two-pair: %.3f\n", proTwo);
        System.out.printf("The probability of One-pair: %.3f\n", proOne);



            }//end of the main
        }//end of the class


